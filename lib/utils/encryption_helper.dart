import 'dart:io';
import 'package:encrypt/encrypt.dart';

class EncryptionHelper {
  static Future<File> encryptFile(File file, String key) async {
    final bytes = await file.readAsBytes();
    final iv = IV.fromLength(16); // 16 bytes for AES-CBC

    final encrypter = Encrypter(AES(
      Key.fromUtf8(key),
      mode: AESMode.cbc,
    ));

    final encrypted = encrypter.encryptBytes(bytes, iv: iv);
    final encryptedFile = File('${file.path}.encrypted');
    await encryptedFile.writeAsBytes(encrypted.bytes);
    return encryptedFile;
  }

  static Future<File> decryptFile(File file, String key) async {
    final bytes = await file.readAsBytes();
    final iv = IV.fromLength(16); // 16 bytes for AES-CBC

    final decrypter = Encrypter(AES(
      Key.fromUtf8(key),
      mode: AESMode.cbc,
    ));

    final decrypted = decrypter.decryptBytes(Encrypted(bytes), iv: iv);
    final decryptedFile = File(file.path.replaceFirst('.encrypted', ''));
    await decryptedFile.writeAsBytes(decrypted);
    return decryptedFile;
  }
}
