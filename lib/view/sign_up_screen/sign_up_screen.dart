import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../providers/auth_provider.dart';

class SignUpScreen extends StatelessWidget {
  static const route = '/sign_up_screen';
  const SignUpScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Registration'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Provider.of<LoginProvider>(context, listen: false).signInWithOTP(
                '123456',
                context: context); // Enter the OTP for testing
          },
          child: const Text('Sign In'),
        ),
      ),
    );
  }
}
