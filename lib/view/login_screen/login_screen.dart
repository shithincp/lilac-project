import 'package:flutter/material.dart';
import 'package:lilac/view/sign_up_screen/sign_up_screen.dart';
import 'package:provider/provider.dart';

import '../../providers/auth_provider.dart';

class LoginScreen extends StatelessWidget {
  static const route = '/login_screen';
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Provider.of<LoginProvider>(context, listen: false)
                .verifyPhoneNumber('+917558000505');
            Navigator.pushNamed(context, SignUpScreen.route);
          },
          child: const Text('Get OTP'),
        ),
      ),
    );
  }
}
