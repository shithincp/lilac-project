import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lilac/view/home_screen/home_screen.dart';
import 'package:lilac/view/login_screen/login_screen.dart';
import '../../utils/colors.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});
  static const route = '/splash_screen';

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorFFFFFF,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Text(
              'Lilac',
              style: GoogleFonts.rubik(
                fontSize: 28,
                fontWeight: FontWeight.w600,
                color: color000000,
              ),
            ),
          ),
        ],
      ),
    );
  }

  _init() async {
    // await LocationPermissionHandler.checkPermission();
    // await FCM.requestPermission();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      // MasterProvider masterProvider =
      //     Provider.of<MasterProvider>(context, listen: false);
      // AuthProvider authProvider =
      //     Provider.of<AuthProvider>(context, listen: false);
      // masterProvider.selectedIndex = 2;
      // authProvider.socialEmail = '';
      // authProvider.socialName = '';
    });

    Timer(
      const Duration(seconds: 3),
      () async {
        await _checkLogin();
      },
    );
  }

  Future<void> _checkLogin() async {
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser == null) {
      Navigator.pushAndRemoveUntil(
          context,
          CupertinoPageRoute(builder: (context) => const LoginScreen()),
          (route) => false);
      return;
    } else {
      Navigator.pushNamedAndRemoveUntil(
          context, HomeScreen.route, (route) => false);
    }
  }
}
