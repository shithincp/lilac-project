import 'dart:developer';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerScreen extends StatefulWidget {
  static const route = '/video_player';

  const VideoPlayerScreen({super.key});

  @override
  _VideoPlayerScreenState createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  VideoPlayerController? _controller;
  String? _localVideoPath;

  @override
  void initState() {
    super.initState();
    _initVideo();
  }

  void _initVideo() async {
    File? dowloadedFile;
    String videoUrl =
        'https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4';
    // Check if video is already downloaded
    if (_localVideoPath == null) {
      // Replace with code to download video from cloud storage

      String videoFileName = videoUrl.split('/').last; // Name of the video file

      // Download video
      dowloadedFile = await _downloadVideo(videoUrl, videoFileName);
    }

    // Encrypt downloaded video
    String encryptionKey = '0123456789ABCDEF0123456789ABCDEF';
    await _encryptVideo(encryptionKey);

    // Decrypt and play video
    final decryptedFile = await _decryptVideo(encryptionKey);
    if (!decryptedFile.existsSync()) {
      log('Video file does not exist at ${decryptedFile.path}');
      return;
    }

    // if (dowloadedFile != null) {
    //   _controller = VideoPlayerController.file(dowloadedFile);
    //   _controller?.addListener(() {
    //     final bool isPlaying = _controller!.value.isPlaying;
    //     if (_controller!.value.hasError) {
    //       log('Video player error: ${_controller!.value.errorDescription}');
    //       log('Video player stack trace: ${decryptedFile.path}');
    //     }
    //   });
    // } else
    {
      _controller = VideoPlayerController.networkUrl(Uri.parse(
          'https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4'));
    }
    _controller?.initialize().then((_) {
      setState(() {});
      _controller?.play();
    });
  }

  Future<File> _downloadVideo(String videoUrl, String fileName) async {
    Directory? appDocDir = await getDownloadsDirectory();
    String appDocPath = appDocDir!.path;
    String filePath = '$appDocPath/$fileName';
    log('filePath------$filePath');
    // Download video using Dio package
    Dio dio = Dio();
    await dio.download(videoUrl, filePath);

    setState(() {
      _localVideoPath = filePath;
    });
    log('_localVideoPath---$_localVideoPath');
    return File(filePath);
  }

  File? enryptedFile;
  Future<void> _encryptVideo(String encryptionKey) async {
    final file = File(_localVideoPath!);
    final bytes = await file.readAsBytes();

    final iv = encrypt.IV
        .fromLength(16); // IV length for AES-CBC is typically 16 bytes
    final encrypter = encrypt.Encrypter(encrypt
        .AES(encrypt.Key.fromUtf8(encryptionKey), mode: encrypt.AESMode.cbc));

    final encrypted = encrypter.encryptBytes(bytes, iv: iv);
    enryptedFile = await file.writeAsBytes(encrypted.bytes);
  }

  Future<File> _decryptVideo(String encryptionKey) async {
    final file = File(_localVideoPath!);
    final bytes = await file.readAsBytes();

    final iv = encrypt.IV
        .fromLength(16); // IV length for AES-CBC is typically 16 bytes
    final decrypter = encrypt.Encrypter(encrypt
        .AES(encrypt.Key.fromUtf8(encryptionKey), mode: encrypt.AESMode.cbc));

    final decryptedBytes = decrypter.decryptBytes(
      encrypt.Encrypted(bytes),
      iv: iv,
    );

    final decryptedFile = File('${file.path}');
    await decryptedFile.writeAsBytes(decryptedBytes);

    return decryptedFile;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Video Player'),
      ),
      body: Center(
        child: _controller != null && _controller!.value.isInitialized
            ? Container(
                padding: const EdgeInsets.all(20),
                child: AspectRatio(
                  aspectRatio: _controller!.value.aspectRatio,
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      VideoPlayer(_controller!),
                      ClosedCaption(text: _controller!.value.caption.text),
                      _ControlsOverlay(controller: _controller!),
                      VideoProgressIndicator(_controller!,
                          allowScrubbing: true),
                    ],
                  ),
                ),
              )
            : const CircularProgressIndicator(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          String videoUrl =
              'https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4';
          String videoFileName = videoUrl.split('/').last;
          await _downloadVideo(videoUrl, videoFileName);
          var snackBar = const SnackBar(content: Text('Downloading'));
          if (!mounted) return;
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        },
        child: const Icon(Icons.download),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller?.dispose();
  }
}

class _ControlsOverlay extends StatefulWidget {
  const _ControlsOverlay({required this.controller});

  static const List<Duration> _exampleCaptionOffsets = <Duration>[
    Duration(seconds: -10),
    Duration(seconds: -3),
    Duration(seconds: -1, milliseconds: -500),
    Duration(milliseconds: -250),
    Duration.zero,
    Duration(milliseconds: 250),
    Duration(seconds: 1, milliseconds: 500),
    Duration(seconds: 3),
    Duration(seconds: 10),
  ];
  static const List<double> _examplePlaybackRates = <double>[
    0.25,
    0.5,
    1.0,
    1.5,
    2.0,
    3.0,
    5.0,
    10.0,
  ];

  final VideoPlayerController controller;

  @override
  State<_ControlsOverlay> createState() => _ControlsOverlayState();
}

class _ControlsOverlayState extends State<_ControlsOverlay> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 50),
          reverseDuration: const Duration(milliseconds: 200),
          child: widget.controller.value.isPlaying
              ? const SizedBox.shrink()
              : const ColoredBox(
                  color: Colors.black26,
                  child: Center(
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 100.0,
                      semanticLabel: 'Play',
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            widget.controller.value.isPlaying
                ? widget.controller.pause()
                : widget.controller.play();
            setState(() {});
          },
        ),
        Align(
          alignment: Alignment.topLeft,
          child: PopupMenuButton<Duration>(
            initialValue: widget.controller.value.captionOffset,
            tooltip: 'Caption Offset',
            onSelected: (Duration delay) {
              widget.controller.setCaptionOffset(delay);
            },
            itemBuilder: (BuildContext context) {
              return <PopupMenuItem<Duration>>[
                for (final Duration offsetDuration
                    in _ControlsOverlay._exampleCaptionOffsets)
                  PopupMenuItem<Duration>(
                    value: offsetDuration,
                    child: Text('${offsetDuration.inMilliseconds}ms'),
                  )
              ];
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(
                // Using less vertical padding as the text is also longer
                // horizontally, so it feels like it would need more spacing
                // horizontally (matching the aspect ratio of the video).
                vertical: 12,
                horizontal: 16,
              ),
              child: Text(
                  '${widget.controller.value.captionOffset.inMilliseconds}ms'),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: PopupMenuButton<double>(
            initialValue: widget.controller.value.playbackSpeed,
            tooltip: 'Playback speed',
            onSelected: (double speed) {
              widget.controller.setPlaybackSpeed(speed);
            },
            itemBuilder: (BuildContext context) {
              return <PopupMenuItem<double>>[
                for (final double speed
                    in _ControlsOverlay._examplePlaybackRates)
                  PopupMenuItem<double>(
                    value: speed,
                    child: Text('${speed}x'),
                  )
              ];
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(
                // Using less vertical padding as the text is also longer
                // horizontally, so it feels like it would need more spacing
                // horizontally (matching the aspect ratio of the video).
                vertical: 12,
                horizontal: 16,
              ),
              child: Text('${widget.controller.value.playbackSpeed}x'),
            ),
          ),
        ),
      ],
    );
  }
}
