// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:video_player/video_player.dart';

// class VideoPlayerWidget extends StatefulWidget {
//   final String videoUrl;

//   const VideoPlayerWidget({Key? key, required this.videoUrl}) : super(key: key);

//   @override
//   _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState();
// }

// class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
//   VideoPlayerController? _controller;

//   @override
//   void initState() {
//     super.initState();
//     _checkLocalFile(widget.videoUrl); // Check for local encrypted video first
//   }

//   Future<void> _checkLocalFile(String url) async {
//     final fileName = url.split('/').last;
//     final directory = await getApplicationDocumentsDirectory();
//     final localFile = File('${directory.path}/$fileName');

//     if (await localFile.exists()) {
//       final decryptedBytes =
//           await decryptFile(localFile, key); // Decrypt before playing
//       _controller = VideoPlayerController.memory(decryptedBytes);
//       await _controller?.initialize();
//       setState(() {});
//     } else {
//       // Download and encrypt the video if not found locally
//       final downloadedFile = await downloadVideo(url);
//       _controller = VideoPlayerController.file(downloadedFile);
//       await _controller?.initialize();
//       setState(() {});
//     }
//   }

//   @override
//   void dispose() {
//     _controller?.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return _controller != null
//         ? VideoPlayer(_controller!)
//         : const Center(child: CircularProgressIndicator());
//   }
// }
