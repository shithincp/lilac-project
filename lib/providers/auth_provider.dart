import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:lilac/view/home_screen/home_screen.dart';
import 'package:lilac/view/splash_screen/splash_screen.dart';

class LoginProvider with ChangeNotifier {
  final FirebaseAuth auth = FirebaseAuth.instance;
  late User? _user;
  late AuthCredential _phoneAuthCredential;

  User? get user => _user;

  Future<void> verifyPhoneNumber(String phoneNumber) async {
    await auth.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      timeout: const Duration(seconds: 60),
      verificationCompleted: (PhoneAuthCredential credential) async {
        await auth.signInWithCredential(credential);
      },
      verificationFailed: (FirebaseAuthException e) {
        print(e.message);
      },
      codeSent: (String verificationId, int? resendToken) {
        // Save verificationId and resendToken
        _phoneAuthCredential = PhoneAuthProvider.credential(
          verificationId: verificationId,
          smsCode: '123456', // Automatically enter the OTP for testing
        );
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        // Timeout logic
      },
    );
  }

  Future<void> signInWithOTP(String smsCode,
      {required BuildContext context}) async {
    try {
      await auth.signInWithCredential(_phoneAuthCredential);

      _user = auth.currentUser!;
      if (_user != null) {
        _user?.updateDisplayName('Shithin');
        if (!context.mounted) return;

        Navigator.pushNamed(context, HomeScreen.route);
      }
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  Future<void> signOut({required BuildContext context}) async {
    await auth.signOut();
    _user = null;
    // if (!context.mounted) return;
    Phoenix.rebirth(context);
    Navigator.pushNamedAndRemoveUntil(
        context, SplashScreen.route, (route) => false);
    notifyListeners();
  }
}
