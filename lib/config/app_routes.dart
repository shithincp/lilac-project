import 'package:flutter/material.dart';
import 'package:lilac/view/home_screen/home_screen.dart';
import 'package:lilac/view/video_screen/video_screen.dart';
import '../view/login_screen/login_screen.dart';
import '../view/sign_up_screen/sign_up_screen.dart';
import '../view/splash_screen/splash_screen.dart';
import 'slide_right_route.dart';

RouteFactory onAppGenerateRoute() => (settings) {
      switch (settings.name) {
        case SplashScreen.route:
          return SlideRightRoute(const SplashScreen(), settings.name);
        case LoginScreen.route:
          return SlideRightRoute(const LoginScreen(), settings.name);
        case HomeScreen.route:
          return SlideRightRoute(const HomeScreen(), settings.name);
        case SignUpScreen.route:
          return SlideRightRoute(const SignUpScreen(), settings.name);
        case VideoPlayerScreen.route:
          return SlideRightRoute(VideoPlayerScreen(), settings.name);
        default:
          return null;
      }
    };
