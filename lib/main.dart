import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:lilac/config/providers.dart';
import 'package:lilac/firebase_options.dart';
import 'package:media_cache_manager/media_cache_manager.dart';
import 'package:provider/provider.dart';
import 'config/app_routes.dart';
import 'utils/colors.dart';
import 'view/splash_screen/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  await MediaCacheManager.instance.init(
    encryptionPassword: 'video_encryption_key',
    daysToExpire: 1,
  );
  final savedThemeMode = await AdaptiveTheme.getThemeMode();

  runApp(Phoenix(
      child: MultiProvider(
          providers: providers,
          child: MainApp(
            savedThemeMode: savedThemeMode,
          ))));
}

class MainApp extends StatelessWidget {
  final AdaptiveThemeMode? savedThemeMode;
  const MainApp({super.key, this.savedThemeMode});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return AdaptiveTheme(
      light: ThemeData(
        useMaterial3: true,
        brightness: Brightness.light,
        colorSchemeSeed: Colors.blue,
      ),
      dark: ThemeData(
        useMaterial3: true,
        brightness: Brightness.dark,
        colorSchemeSeed: Colors.blue,
      ),
      initial: savedThemeMode ?? AdaptiveThemeMode.light,
      builder: (light, dark) => ScreenUtilInit(
        designSize: const Size(360, 690),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          return MaterialApp(
            theme: light,
            darkTheme: dark,
            debugShowCheckedModeBanner: false,
            title: 'Lilac',
            onGenerateRoute: onAppGenerateRoute(),
            initialRoute: SplashScreen.route,
          );
        },
      ),
    );
  }
}
